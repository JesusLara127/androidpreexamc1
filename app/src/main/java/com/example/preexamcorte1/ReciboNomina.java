package com.example.preexamcorte1;

public class ReciboNomina {
    private String numeroRecibo;
    private String nombre;
    private double horasNormales;
    private double horasExtra;
    private int puesto;
    private double impuesto;

    public static final int PUESTO_AUXILIAR = 1;
    public static final int PUESTO_ALBANIL = 2;
    public static final int PUESTO_INGENIERO_OBRA = 3;
    public static final double PAGO_BASE = 200.0;

    public ReciboNomina(String numeroRecibo, String nombre, double horasNormales, double horasExtra, int puesto) {
        this(numeroRecibo, nombre, horasNormales, horasExtra, puesto, 0.16);
    }

    public ReciboNomina(String numeroRecibo, String nombre, double horasNormales, double horasExtra, int puesto, double impuesto) {
        this.numeroRecibo = numeroRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtra = horasExtra;
        this.puesto = puesto;
        this.impuesto = impuesto;
    }

    public double calcularSubtotal() {
        double pagoBase;
        switch (puesto) {
            case PUESTO_AUXILIAR:
                pagoBase = PAGO_BASE * 1.2;
                break;
            case PUESTO_ALBANIL:
                pagoBase = PAGO_BASE * 1.5;
                break;
            case PUESTO_INGENIERO_OBRA:
                pagoBase = PAGO_BASE * 2.0;
                break;
            default:
                pagoBase = PAGO_BASE;
        }

        double pagoHorasNormales = pagoBase * horasNormales;
        double pagoHorasExtra = pagoBase * horasExtra * 2;

        return pagoHorasNormales + pagoHorasExtra;
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * impuesto;
    }

    // Getters y setters (opcional, si se necesita acceso a los campos)
    public String getNumeroRecibo() {
        return numeroRecibo;
    }

    public void setNumeroRecibo(String numeroRecibo) {
        this.numeroRecibo = numeroRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getHorasNormales() {
        return horasNormales;
    }

    public void setHorasNormales(double horasNormales) {
        this.horasNormales = horasNormales;
    }

    public double getHorasExtra() {
        return horasExtra;
    }

    public void setHorasExtra(double horasExtra) {
        this.horasExtra = horasExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }
}
