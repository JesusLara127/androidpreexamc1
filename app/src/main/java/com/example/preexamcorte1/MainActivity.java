package com.example.preexamcorte1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    private Button btnEnter;
    private Button btnExit;
    private EditText txtUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEnter.setOnClickListener(v -> login());
        btnExit.setOnClickListener(v -> salir());
    }

    private void iniciarComponentes() {
        btnEnter = findViewById(R.id.btnEnter);
        btnExit = findViewById(R.id.btnExit);
        txtUsuario = findViewById(R.id.txtUsuario);
    }

    private void login() {
        String strUsuario = txtUsuario.getText().toString().trim();
        if (!strUsuario.isEmpty()) {
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtra("usuario", strUsuario);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Por favor, ingresa un nombre de usuario", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        finish();
    }
}
